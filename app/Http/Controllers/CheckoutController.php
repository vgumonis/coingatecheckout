<?php
/**
 * Created by PhpStorm.
 * User: Vilius
 * Date: 1/16/2019
 * Time: 9:42 PM
 */

namespace App\Http\Controllers;

use CoinGate\CoinGate;
use CoinGate\Merchant\Order;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class CheckoutController extends Controller
{
    public function __construct()
    {
        CoinGate::config([
            'environment' => env('COIN_GATE_ENV'),
            'auth_token' => env('COIN_GATE_API_KEY'),
            'curlopt_ssl_verifypeer' => env('COIN_GATE_SSL_VERIFY_PEER')
        ]);
    }

    public function createOrder(Request $request)
    {
        $order = Order::create($request->all());
        return response()->json([
            'payment' => $order->__get('payment_url'),
            'id' => $order->__get('id')
        ]);
    }

    public function cancelOrder()
    {
        return redirect('/');
    }

    public function successOrder()
    {
        return view('success-order', ['message' => 'Transaction successful']);
    }

    public function failOrder()
    {
        return view('fail-order', ['message' => 'Transaction failed']);
    }
}