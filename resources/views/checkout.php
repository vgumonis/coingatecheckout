<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>submit demo</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<form>
    Enter Value:<br>
    <input type="text" name="value" id="value" required>
    <br>
</form>
<button class="btn btn-success" type='button' value='Submit form' id="create-order-button"> Pay with Coingate</button>
</body>
</html>

<script language="javascript" type="text/javascript">

    $("#create-order-button").on("click", function (e) {
        if (checkIfEmpty()) {
            return;
        }
        $.ajax({
            type: "POST",
            url: "<?php echo url('/checkout/order/create'); ?>",
            data: {
                order_id: "YOUR-CUSTOM-ORDER-ID-115",
                price_amount: $("#value").val(),
                price_currency: "USD",
                receive_currency: "EUR",
                callback_url: "https://example.com/payments/callback?token=6tCENGUYI62ojkuzDPX7Jg",
                cancel_url: "<?php echo url('/checkout/order/fail/'); ?>",
                success_url: "<?php echo url('/checkout/order/success'); ?>",
                title: "Order #112",
                description: "Apple Iphone 6"
            },
            success: function (response) {
                window.location.href = response.payment;
            },
            error: function (response) {
                console.log(response);
            }
        });
    });

    function checkIfEmpty() {
        if ($("#value").val() == null || $("#value").val() == "") {
            alert("Please enter value");
            return true;
        }
    }

</script>